; Launch PoE and tools/scripts

Run, C:\Users\Sylvain\PoE-Awakened-PoE-Trade\Awakened PoE Trade.exe
Run, C:\Users\Sylvain\Documents\POE-Trades-Companion\POE Trades Companion.ahk
Run, C:\Users\Sylvain\Workspace\auto-hotkey\PoE Flasks\notaflaskmacro.exe
Run, C:\Jeux\XMouseButtonControl\64bit (x64)\XMouseButtonControl.exe
Run, steam://rungameid/238960

;this line makes the following lines only work when Path of Exile is the active window
;#IfWinActive, ahk_class POEWindowClass

;Kill processes when leaving POE
#q::
{
	Process, Close, PathOfExileSteam.exe
	Process, Close, Awakened PoE Trade.exe
	Process, Close, notaflaskmacro.exe
	Process, Close, XMouseButtonControl.exe
	; Can't kill POE Trades Companion :(
	;Process, Close, AutoHotkey.exe
	
	ExitApp
}
