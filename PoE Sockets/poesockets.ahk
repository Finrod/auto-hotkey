﻿; Use crafting bench and filters "sockets" to only show sockets options
; Use numpad 2, 3 and 4 to use crafting bench to set 2, 3 or 4 sockets for off-colors
; Should not break ToS as it only do one server action at a time

; Configured for 2540*1440 pixels

; Changes the tooltip and icon on the TrayIcon
Menu, Tray, Tip, Off colors finger saver
Menu, Tray, Icon, %A_SCRIPTDIR%\chromatic.ico

; this line makes the script only work when Path of Exile is the active window
#IfWinActive, ahk_class POEWindowClass

; numpad 2, 3 and 4 to use crafting bench to set 2, 3 or 4 sockets
Numpad2::
	Click, 800 400
	Click, 800 1200
	return
Numpad3::
	Click, 800 470
	Click, 800 1200
	return
Numpad4::
	Click, 800 540
	Click, 800 1200
	return
