#IfWinExist NoxPlayer

; Delay between clicks
SetControlDelay 200

Icon 	= .\img\icon.ico
IfExist, %Icon%
  Menu, Tray, Icon, %Icon%
 
; Window size
NoxWidth = 540
NoxHeight = 960

; Images
GogMenu = .\img\gog\menu.png
GogStart = .\img\gog\start.png
GogCollect = .\img\gog\collect.png
GogCollect6 = .\img\gog\collect-x6.png
UltiArbrara = .\img\ulti\arbrara.png
UltiBellylarf = .\img\ulti\bellylarf.png
UltiBoomer = .\img\ulti\boomer.png
UltiHilt = .\img\ulti\hilt.png
UltiJim = .\img\ulti\jim.png
UltiLenny = .\img\ulti\lenny.png
UltiLia = .\img\ulti\lia.png
UltiNanna = .\img\ulti\nanna.png
UltiOy = .\img\ulti\oy.png
UltiRedroh = .\img\ulti\redroh.png
UltiRon = .\img\ulti\ron.png
UltiSam = .\img\ulti\sam.png
UltiTam = .\img\ulti\tam.png
UltiUno = .\img\ulti\uno.png
UltiV = .\img\ulti\v.png
UltiVexx = .\img\ulti\vexx.png
UltiWendle = .\img\ulti\wendle.png

; Image search function
SearchImage(Image, NoxWidth, NoxHeight)
{
	if !WinActive("NoxPlayer")
		WinActivate, NoxPlayer

	ImageSearch, MenuX, MenuY, 0, 0, %NoxWidth%, %NoxHeight%, %Image%

	if (ErrorLevel = 0)
		ControlClick, X%MenuX% Y%MenuY%, NoxPlayer
;	else if (ErrorLevel = 1)
;		MsgBox, Not found %Image%
;	else
;		MsgBox, Error %Image%
		
	return ErrorLevel
}

; F7: start aeon farming with ultimate skills
#MaxThreadsPerHotkey 3
F7::
#MaxThreadsPerHotkey 1

; Ask to stop the loop
if Running
{
    Running := false
    return
}

; Start the loop
Running 	:= true
RunId 		:= 1
Loop
{
	; Main menu
	IsStart := SearchImage(GogMenu, NoxWidth, NoxHeight)
	if (IsStart = 0)
	{
		Menu, Tray, Tip, Aeon: %RunId% runs
		RunId++
	}

	; Start GOG
	SearchImage(GogStart, NoxWidth, NoxHeight)

	; Collect GOG
	SearchImage(GogCollect, NoxWidth, NoxHeight)
	SearchImage(GogCollect6, NoxWidth, NoxHeight)

	; Heroes ulti
	SearchImage(UltiArbrara, NoxWidth, NoxHeight)
	SearchImage(UltiBellylarf, NoxWidth, NoxHeight)
	SearchImage(UltiBoomer, NoxWidth, NoxHeight)
	SearchImage(UltiHilt, NoxWidth, NoxHeight)
	SearchImage(UltiJim, NoxWidth, NoxHeight)
	SearchImage(UltiLenny, NoxWidth, NoxHeight)
	SearchImage(UltiLia, NoxWidth, NoxHeight)
	SearchImage(UltiNanna, NoxWidth, NoxHeight)
	SearchImage(UltiOy, NoxWidth, NoxHeight)
	SearchImage(UltiRedroh, NoxWidth, NoxHeight)
	SearchImage(UltiRon, NoxWidth, NoxHeight)
	SearchImage(UltiSam, NoxWidth, NoxHeight)
	SearchImage(UltiTam, NoxWidth, NoxHeight)
	SearchImage(UltiUno, NoxWidth, NoxHeight)
	SearchImage(UltiV, NoxWidth, NoxHeight)
	SearchImage(UltiVexx, NoxWidth, NoxHeight)
	SearchImage(UltiWendle, NoxWidth, NoxHeight)

	Sleep, 2000

    ; Stop the loop
    if not Running
        break
}
; Reset
Running := false
return

; F8: start aeon farming without ultimate skills
#MaxThreadsPerHotkey 3
F8::
#MaxThreadsPerHotkey 1

; Ask to stop the loop
if Running
{
    Running := false
    return
}

; Start the loop
Running 	:= true
RunId 		:= 1
Loop
{
	; Main menu
	IsStart := SearchImage(GogMenu, NoxWidth, NoxHeight)
	if (IsStart = 0)
	{
		Menu, Tray, Tip, Aeon: %RunId% runs
		RunId++
	}

	; Start GOG
	SearchImage(GogStart, NoxWidth, NoxHeight)

	; Collect GOG
	SearchImage(GogCollect, NoxWidth, NoxHeight)
	SearchImage(GogCollect6, NoxWidth, NoxHeight)

	Sleep, 2000

    ; Stop the loop
    if not Running
        break
}
; Reset
Running := false
return
