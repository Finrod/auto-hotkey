﻿; Lines starting with a ;  are comments and are not part of the actual script.
; If you want to deactivate a flask press(e.g. because it is your hp flask) simply add a ;  to the start of the line

; this line makes the script only work when Path of Exile is the active window
#IfWinActive, ahk_class POEWindowClass

; Variables
IniRead, Flask3, notaflaskmacro.ini, flasks, flask3, true
IniRead, Flask4, notaflaskmacro.ini, flasks, flask4, true
IniRead, Flask5, notaflaskmacro.ini, flasks, flask5, true
;Flask3 := true
;Flask4 := true
;Flask5 := true

; Changes the tooltip and icon on the TrayIcon
Menu, Tray, Tip, This is not a flask macro!
Menu, Tray, Icon, %A_SCRIPTDIR%\BottledPurity.ico

; Option menu
Menu, Tray, Add ; separator
Menu, Tray, Add, Toggle flask 3, ToggleFlask3
Menu, Tray, Add, Toggle flask 4, ToggleFlask4
Menu, Tray, Add, Toggle flask 5, ToggleFlask5

; Default states
if (Flask3)
	Menu, Tray, Check, Toggle flask 3
else
	Menu, Tray, Uncheck, Toggle flask 3

if (Flask4)
	Menu, Tray, Check, Toggle flask 4
else
	Menu, Tray, Uncheck, Toggle flask 4

if (Flask5)
	Menu, Tray, Check, Toggle flask 5
else
	Menu, Tray, Uncheck, Toggle flask 5

return

; Menu labels
ToggleFlask3:
	Menu, Tray, ToggleCheck, Toggle flask 3
	Flask3 := !Flask3
	IniWrite, %Flask3%, notaflaskmacro.ini, flasks, flask3
	return

ToggleFlask4:
	Menu, Tray, ToggleCheck, Toggle flask 4
	Flask4 := !Flask4
	IniWrite, %Flask4%, notaflaskmacro.ini, flasks, flask4
	return

ToggleFlask5:
	Menu, Tray, ToggleCheck, Toggle flask 5
	Flask5 := !Flask5
	IniWrite, %Flask5%, notaflaskmacro.ini, flasks, flask5
	return

; ctrl+numpad to toggle flask usage
^Numpad3::
	Menu, Tray, ToggleCheck, Toggle flask 3
	Flask3 := !Flask3
	IniWrite, %Flask3%, notaflaskmacro.ini, flasks, flask3
	return
^Numpad4::
	Menu, Tray, ToggleCheck, Toggle flask 4
	Flask4 := !Flask4
	IniWrite, %Flask4%, notaflaskmacro.ini, flasks, flask4
	return
^Numpad5::
	Menu, Tray, ToggleCheck, Toggle flask 5
	Flask5 := !Flask5
	IniWrite, %Flask5%, notaflaskmacro.ini, flasks, flask5
	return


; The key (or mouse button) you press to activate the script. For a list of supported "keys" and combinations, see https://autohotkey.com/docs/Hotkeys.htm
; XButton1 = "Back"-Button on my mouse. For a complete list of special keys, see https://autohotkey.com/docs/KeyList.htm
; SC003 is the "é/2" key
SC003::
{
	; Initialize random delays between 57 and 114 ms (arbitrary values, may be changed)
	random, delay3, 57, 114
	random, delay4, 57, 114
	random, delay5, 57, 114

	send, é ; simulates the keypress of the 2 button

	if (Flask3) {
		sleep, %delay3%
		send, " ; simulates the keypress of the 3 button
	}

	if (Flask4) {
		sleep, %delay4%
		send, ' ; simulates the keypress of the 4 button
	}

	if (Flask5) {
		sleep, %delay5%
		send, ( ; simulates the keypress of the 5 button
	}
}
return

